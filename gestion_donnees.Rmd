---
title: "La gestion des données"
author: "Facundo Muñoz<br/>facundo.munoz@cirad.fr<br/>"
institute: "<img src=\"img/CirBlanc_L230px.png\" style=\"width: 25%\" align=\"top\" />"
output:
  xaringan::moon_reader:
    # css: ["default", "assets/css/my-theme.css", "assets/css/my-fonts.css"]
    css: ["default", "default-fonts", "libs/font-awesome/css/fontawesome-all.min.css", "assets/css/cirad.css"]
    seal: false
    lib_dir: libs
    nature:
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      # scale images
      beforeInit: "macros.js"
      ratio: "16:9"
    fig_widht: 3
    fig_height: 2
---


```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(collapse = TRUE, fig.retina = 3)
library(xaringanExtra)
xaringanExtra::use_share_again()
xaringanExtra::style_share_again(
  share_buttons = c("twitter", "linkedin", "pocket")
)
```

```{r broadcast, echo=FALSE}
xaringanExtra::use_broadcast()
```



class: title-slide, inverse

.pull-left[
# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` 

![](img/CirBlanc_L230px.png)
]
.pull-right[
![](https://datacarpentry.org/rr-organization1/fig/files_messy_tidy.png)
]




???

<!-- --- -->
<!-- layout: true -->

<!-- <a class="footer-link" href="https://umr-astre.pages.mia.inra.fr/training/notions_stats/">Notions de base en statistiques - umr-astre.pages.mia.inra.fr/training/notions_stats/</a> -->


---
class: inverse, middle, center

# Le stockage de données


---
# Quelques problèmes courants

- J'ai plusieurs __copies des données__ mais je ne suis pas sûr quelle est la dernière version.

--

- J'ai les données, mais je ne suis pas sûr d'avoir la __dernière version__.

--

- J'ai un fichier de données mais je ne me souviens pas de ce qu'il __contient__.


--

- J'ai des données, mais je ne les __retrouve__ pas.

--

- Je ne sais pas comment interpréter certaines des __variables__.

--

- J'ai corrigé des erreurs et on m'a envoyé une __nouvelle version__ basée sur les données originales

---
background-image: url(https://media.giphy.com/media/TKvErZACqjawXcTMSP/giphy.gif)
background-size: cover
class: middle, inverse

# Pertes de temps

# Erreurs inaperçues


---
# Quelques principes

1. ~~Limiter~~ Éviter la __duplication__ des données

--

2. Gérer les __versions__

--

3. __Documenter__ les données (méta-données)

--

4. Adopter une convention pour les __noms des fichiers__


.center[
  # Entamez un document de _guidelines_ à respecter
]

???

- Guide pour nos actions. Pas toujours possible, mais peuvent nous aider à prendre des décisions.

- Mieux se référer à un dépôt central de données qu'envoyer des copies par é-mail

- Les données peuvent évoluer (ajouter, corriger). Utilisez __un__ système quelconque. Soit un outil, ou une procédure.
Une date dans le nom du fichier peut être suffisant. Mais il faut être __consistent__ et __systèmatique__. 

- Un fichier de texte à coté, par exemple, ou un outil (dépôt, cf. 1). Description des variables, source des données, etc.

- Réfléchissez au noms des fichiers (et variables) pour qu'ils aient du sens, qui reflètent leur contenu, qui soient parlants. 

Chaque situation est différente, je ne peux pas vous proposer un système qui vous convient à tous. 

__Le choix du système n'est pas aussi important que d'en avoir un.__


---
background-image: url(img/spreadsheet.png)
background-size: 50%
background-position: right

# Feuilles de calcul

.pull-left[
Combinent : 

- stockage de données

- entrée de données

- visualisation (tables, format)

- analyses (formules, conditions, résultats, résumés, etc.)

- figures
]

???

- J'adore les feuilles de calcul, je pense sincèrement que sont un outil fantastique

- Mais je ne les utilise pas ni pour le stockage ni pour la manipulation de données



---
class: inverse, center, middle

# Les besoins pour l'__entree__, le __stockage__ et la __visualisation__ de données sont fondamentalement différents

![](img/example_excel.png)

???

- Les données stockées doivent être touchés le moins possible pour minimiser le risque de modification accidentelle

---
# Histoires d'horreur

- MS Excel interprète les dates et les garde internement comme un nombre... avec des conventions différents pour Mac et Windows [🔗](https://uc3.cdlib.org/2014/04/09/abandon-all-hope-ye-who-enter-dates-in-excel/)


- MS Excel interprète _automatiquement_ certaines textes comme des dates. E.g. le symbole pour le gène "Oct-4" peut être écrasé sans notification.

  [Un étude de 2016](https://doi.org/10.1186/s13059-016-1044-7) à trouvé ce type d'__erreurs dans 20 %__ des listes des gènes publiées.



???

- Excel a la mauvaise habitude d'interpréter les données (pour améliorer la visualisation) mais en les __changeant__ au passage.

- L'interprétation des données peuvent dépendre du système d'exploitation, du langage du système et d'autres paramètres.

---
background-image: url(img/gene_renames.png)
background-size: contain
background-position: center


---
class: middle, center
background-image: url(img/and-thats-why-excel.jpg)
background-size: contain
background-position: center


---
class: inverse, center, middle

# Vous allez utiliser les outils que vous __maîtrisez__, pas forcement ceux dont vous avez __besoin__

--

Vous pouvez les utiliser, mais pensez aux __principes__ de gestion de données

???

Ok. C'est normal. Je comprends.

Vous pouvez faire un analyse sur Excel, mais faites-les sur une feuille à part. Ne touchez pas les données. C'est mieux d'utiliser un fichier à part (viol de la duplication de données !!, bon... tant pis). 


---
class: inverse, center, middle

# Noms des fichiers (et variables)

.pull-right[.quote[
> ‘There are only two hard things in Computer Science: cache invalidation and naming things.’
>
>    - Phil Karlton
]
]

---
background-image: url(http://phdcomics.com/comics/archive/phd101212s.gif)
background-position: center

---

# No

- myabstract.docx
- Les noms de fichiers d'Agnès utilisent les espaces et la  ponctuation.xlsx
- figure I.png
- fig 2.png
- JW7d^(2sl@nepassupprimerWx2*.txt

--

# Oui

- 2014-06-08_abstract-for-sla.docx
- les-fichiers-dagnes-vont-mieux.xlsx
- fig01_nuage-points-taille-vs-interet.png
- fig02_histogramme-participation.png
- 1986-01-28_donnees-brutes-projet-code.txt

---
# 3 principes pour les noms (de fichiers)

1. Lisibles par une machine

--

2. Lisibles par les humains

--

3. Fonctionne bien avec l'ordre d'affichage

???

- Éviter accents, ponctuation, espaces.

- Utiliser une structure régulière, systématique

- délimiter mots et "groupes" avec - et _

- Dates en format standard ISO-8601

- machine : facilite la recherche de fichiers, et l'extraction de méta-données à partir des noms de fichiers.

- humain : contient info sur les contenus



---
background-image: url(https://imgs.xkcd.com/comics/iso_8601_2x.png)
background-size: contain

.pull-right[
  [Format standard ISO 8601](https://en.wikipedia.org/wiki/ISO_8601)
]

---
class: middle, center

# Excellents noms de fichiers

![](https://datacarpentry.org/rr-organization1/fig/awesome_names.png)

.credit[[datacarpentry]()]
---
# Nommer des fichiers (et des variables)

Des outils d'aide pour développer une __nomenclature__ de fichiers adaptée à vos besoins :

- Kristin Briney (2020) [File naming convention worksheet](http://dataabinitio.com/?p=976)

- [Minnesota Historical Society](https://www.mnhs.org/preserve/records/electronicrecords/erfnaming.php#planning)

- [University of Edinburgh, Records Management](https://www.ed.ac.uk/records-management/guidance/records/practical-guidance/naming-conventions)

---
class: middle, center, inverse

# Organiser ses fichiers

---
background-image: url(https://datacarpentry.org/rr-organization1/fig/files_messy_tidy.png)
background-position: center

.credit[datacarpentry]

---
# Mon structure de projet basique

.left-column[
```{r eval=FALSE}
.
├── data/
├── doc/
├── reports/
├── src/
└── Readme.md
```
]

???

Celle-là est la mienne. Pas forcement adapté à vos besoins.
C'est un exemple.

Selon le cas, il peut y avoir des répertoires ou fichiers supplémentaires.

E.g. raw_data, derived_data, img/figures, paper

---
# Principes

1. Je ne modifie __jamais__ les fichiers dans `data`

2. Toute la __documentation__ à lire dans `doc` (e.g. description des données, articles, etc.)

3. Le travail d'__analyse__ se passe dans `src`

4. Les __résultats__ dans `reports`


???

Peu importe le système. Mais en avoir un.


---
class: inverse, middle, center

# Organisation des données


---
# Principes

## Faciliter :

1. l'__importation__ et le __traitement__ des données avec des différents méthodes et outils

2. la compréhension de la __structure__ des données

---
class: middle, center, inverse

Quelques mauvaises habitudes


---
background-image: url(https://datacarpentry.org/spreadsheet-ecology-lesson/fig/2_datasheet_example.jpg)
background-size: 800px
background-position: center

# Utiliser plusieurs tables

???

C'est un cauchemar à importer !!

C'est bien pour visualiser et pour entrer des données, mais là, on parle de stockage de données. Séparons les choses !!




---
# Utiliser le format des cellules pour codifier de l'information

.pull-left[
![](https://datacarpentry.org/spreadsheet-ecology-lesson/fig/formatting.png)
]

.pull-right[
![](https://datacarpentry.org/spreadsheet-ecology-lesson/fig/good_formatting.png)
]

???

Cette information n'est pas récupéré si on lit les données dans R, par exemple.
Toute information doit être codée dans une __variable__

Encore une fois, on mélange stockage et visualisation.


---
background-image: url(https://www.tandfonline.com/na101/home/literatum/publisher/tandf/journals/content/utas20/2018/utas20.v072.i01/00031305.2017.1375989/20180424-01/images/medium/utas_a_1375989_f0002_b.gif)
background-size: 600px
background-position: center

# Cellules vides

???

- Confondre zéros et valeurs manquantes

- Vide = valeur précedante 

- Agréger des entêtes qui concernent plusieurs lignes/colonnes

Mélange stockage et visualisation


---
background-image: url(https://www.tandfonline.com/na101/home/literatum/publisher/tandf/journals/content/utas20/2018/utas20.v072.i01/00031305.2017.1375989/20180424-01/images/medium/utas_a_1375989_f0005_b.gif)
background-size: 600px
background-position: center

# Structures non-rectangulaires

???

Encore...


---
# Bonnes pratiques

- Garder la __cohérence__ (codifications, majuscules, format, ...)

- Standard ISO 8601 pour les __dates__ (YYYY-MM-DD)

- __Décrire__ les variables (méta-données)

- Données __séparés__ des calculs dérivés

- __Noms__ parlants, courts et descriptifs

- __Sauvegarder__

- Stocker les données en fichiers de __texte__

.credit[[Broman KW, Woo KH (2018)](https://doi.org/10.1080/00031305.2017.1375989)]




---
class: inverse, center, middle

# Tidy data

.credit[[openscapes](https://www.openscapes.org/blog/2020/10/12/tidy-data/)]

???

En plus de respecter ces pratiques, il y a une méthode d'organisation de données pour qui repose sur les principes initiales.


---
background-image: url(https://www.openscapes.org/img/blog/tidydata/tidydata_1.jpg)
background-position: center
class: bottom, right

.credit[[Julie Lowndes and Allison Horst](https://www.openscapes.org/blog/2020/10/12/tidy-data/)]




---
background-image: url(https://www.openscapes.org/img/blog/tidydata/tidydata_2.jpg)
background-position: center
class: bottom, right

.credit[[Julie Lowndes and Allison Horst](https://www.openscapes.org/blog/2020/10/12/tidy-data/)]


???


Cette structure facilite la compréhension et le traitement des données.

On évite des procédures et traitements ad-hoc, selon les pelucliarités de chaque situation.



---
background-image: url(https://www.openscapes.org/img/blog/tidydata/tidydata_3.jpg)
background-position: center
class: bottom, right

.credit[[Julie Lowndes and Allison Horst](https://www.openscapes.org/blog/2020/10/12/tidy-data/)]


???

Cela nous permet d'utilise des outils standard, prévus pour des données rectangulaires.
On apprend à maîtriser les variables, les colonnes, et les observations, les lignes.
On peut filtrer, séléctionner, combiner, résumer, calculer.

Les méthodes et outils nous servent toujours.


---
background-image: url(https://www.openscapes.org/img/blog/tidydata/tidydata_4.jpg)
background-position: center
class: bottom, right

.credit[[Julie Lowndes and Allison Horst](https://www.openscapes.org/blog/2020/10/12/tidy-data/)]


???

Facilite aussi la collaboration.

---
background-image: url(https://www.openscapes.org/img/blog/tidydata/tidydata_5.jpg)
background-position: center
class: bottom, right

.credit[[Julie Lowndes and Allison Horst](https://www.openscapes.org/blog/2020/10/12/tidy-data/)]


???

Facilite l'automatisation, et l'enchaînement de traitements et analyses, la mise à jour, la correction d'erreurs, etc.



---
class: inverse, middle, center

# La description d'un jeu de données


---
class: inverse, middle, center

.quote[
  .pull-right[
    » Votre collaborateur les plus important êtes vous-même dans le passé.
    Mais il ne répond pas à vos é-mails.
  ]
]

---

# Principes

1. Pouvoir __transmettre__ des données avec toute les infos nécessaires pour travailler avec

2. Éviter des __erreurs__ d'interprétation


???

"Transmettre" à une autre personne ou à soit même dans le futur !

"Votre collaborateur le plus important c'est vous même dans le passé, mais il ne répond pas aux é-mails




---
# Méta-données

- Ficher Readme / Lisez-moi

  .small[Description succincte du projet, la source des données et méthodes de collecte, le contexte, les objectifs, références, contactes, etc.]

--

- Description de variables (_data-dictionary_)

  .small[
    - Le nom de la variable tel qu'il apparaît
    - Version du nom adaptée pour visualisations
    - Type de variable (catégorielle, quantitative, etc.)
    - Unités de mesure
    - Rang possible de variation, valeurs possibles, 
    - Description
  ]




---
background-image: url(https://www.tandfonline.com/na101/home/literatum/publisher/tandf/journals/content/utas20/2018/utas20.v072.i01/00031305.2017.1375989/20180424-01/images/medium/utas_a_1375989_f0009_b.gif)
background-position: center
background-size: 600px

# Dictionnaire de données

C'est en soi un jeu de données !! E.g. :




---

# Conclusions

- Très __vagues suggestions__, plusieurs choix et options

- Peu importe le __système__ ou les outils : 

  - Fichiers de texte associés
  
  - Dépôt avec des métadonnées intégrées (e.g. Dataverse)
  
  - Formats spécifiques
  
  - Plan de Gestion de Données

- Choisir un et respecter les __principes__




---
# Références

- Kristin Briney (2020) [File naming convention worksheet](http://dataabinitio.com/?p=976)

- Data Carpentry (2018) [lesson on file organisation](https://datacarpentry.org/rr-organization1/)

- https://www.openscapes.org/blog/2020/10/12/tidy-data/

- https://datacarpentry.org/spreadsheet-ecology-lesson/

- Karl W. Broman & Kara H. Woo (2018). Data organisation in Spreadsheets. _The American Statistician_, 72:1, 2-10, [DOI: 10.1080/00031305.2017.1375989](https://doi.org/10.1080/00031305.2017.1375989)

- Wickham, H (2014). Tidy Data. Journal of Statistical Software 58 (10). [jstatsoft.org/v59/i10/](http://www.jstatsoft.org/v59/i10/)

  

???

---
class: middle, inverse


# Merci!

Diapositives créées à l'aide du package R [**xaringan**](https://github.com/yihui/xaringan).

En s'appuyant sur [remark.js](https://remarkjs.com), [**knitr**](https://yihui.org/knitr), et [RMarkdown](https://rmarkdown.rstudio.com).

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
