# Gestion de données

Diapositives (57) pour une présentation sur les bonnes pratiques de gestion
de données et de documents de la recherche.

Langue : Français.

Thèmes : 

- Stocker et organiser les fichiers

- Organiser les données dans les fichiers

- Documenter les données

- Utiliser le Dataverse Cirad


[![](img/frontpage.png)](https://umr-astre.pages.mia.inra.fr/presentations/gestion-donnees/)

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
